// Rotational Cipher

function rotCipher13(text) {

  let str1 = text.split(" ");
  let key = str1[0].match(/\d+/);
  key = new Number(key[0]);

  let str = text.substring(5);
  var lowerCase = str.toLowerCase();
  // console.log(lowerCase);
  var alphabets = "abcdefghijklmnopqrstuvwxyz".split('');
  // console.log(alphabets);
  var newStr = '';

  for (i = 0; i < lowerCase.length; i++) {
    let currentLetter = lowerCase[i];
    if (currentLetter === ' ') {
      newStr += currentLetter;
      continue;
    }
    if (currentLetter === '`') {
      newStr += '`';
      continue;
    }
    if (currentLetter === '.') {
      newStr += '.';
      continue;
    }

    let currentIndex = alphabets.indexOf(currentLetter);
    let newIndex = currentIndex + key;
    if (newIndex > 25) newIndex = newIndex - 26;
    if (newIndex < 0) newIndex = newIndex + 26;
    if (str[i] === str[i].toUpperCase()) {
      newStr += alphabets[newIndex].toUpperCase();
    }
    else newStr += alphabets[newIndex];
  }

  return  text + ' '+ "gives" + ' ' + newStr;
}

rotCipher13('ROT13 `Gur dhvpx oebja sbk whzcf bire gur ynml qbt.`')

