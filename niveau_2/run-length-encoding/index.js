// Run Length Encoding

function runLengthEncodingDecoding(str) {
  let result = '';
  let result2 = '';
  let count = 0;

  for (i = 0; i < str.length; i++) {
    count++;
    if (str[i] != str[i + 1]) {
      if (count > 1) {
        result = result + count + str[i];
      } else {
        result = result + str[i];
      }
      count = 0;
    }
  }
  // return result;

  for (j = 0; j < result.length; j++) {
    if(result[j] == parseInt(result[j])){
      // console.log(result[j] + ' ' + 'it is number')
      result2 += result[j+1].repeat(result[j]);
      // console.log(result2 + 'result if number')
      j++;
    }
    else if(result[j] !== result2){
      result2 += result[j];
    }
  }
  return str + ' ' + '->' + ' ' + result + ' ' + '->' + ' ' + result2;
}

// runLengthEncoding('AABCCCDEEEE'); //2AB3CD4E
runLengthEncodingDecoding('AABCCCDEEEE');